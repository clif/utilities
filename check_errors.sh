#!/bin/bash

# Reads a quickstats report from the standard input, and checks for each
# provided action type whether the number of errors is less than a given
# threshold.
# args: action_type errors_number_threshold ...
# exit code: number of action types whose number of errors is greater than
# the threshold.

EXIT_CODE=0
ALLSTATS=$(cat)

while test $# -ge 2
do
	STATS=$(echo "$ALLSTATS" | grep "$1" | tr -d ' ')
	ERRORS=`echo "$STATS" | cut -f9`
	if test -n "$ERRORS" -a $ERRORS -gt $2
	then
		echo "$1: $ERRORS requests in error"
		EXIT_CODE=$(($EXIT_CODE+$ERRORS))
	fi
	shift 2
done

exit $EXIT_CODE
