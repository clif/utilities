#!/bin/bash

# Kills all local CLIF servers involved in given test plans.
# Note: the CLIF registry entries associated to the killed CLIF servers are NOT
# cleaned.

usage()
{
    printf "Usage: %s <clif_testplan_file.ctp>...\n" "`basename $0`"
}

if test $# -eq 0
then
    printf "Missing required arguments: test plan file(s).\n" >&2
    usage "$0"
    exit -2
elif test "$1" = -h -o "$1" = --help
then
    usage "$0"
    exit 0
fi

clifcmd listservers "$@" | while read s
do
    if test "$s" != "local host"
    then
        echo "$s"
        pkill -e -f ClifServerImpl\ "$s"
        rm -f "$s.out" "$s.err"
    fi
done
