#!/bin/bash

# Reads a quickstats report from the standard input, and checks for each
# provided action type, whether the response times median is less than a given
# threshold.
# args: action_type responsetime_median_threshold ...
# exit code: number of action types whose response times median exceeds
# the threshold.

EXIT_CODE=0
ALLSTATS=$(cat)

while test $# -ge 2
do
	STATS=$(echo "$ALLSTATS" | grep "$1" | tr -d ' ')
	MEDIAN=`echo "$STATS" | cut -f6`
	if test -n "$MEDIAN" -a test $MEDIAN != NaN -a $MEDIAN -gt $2
	then
		echo "$1: response time median ${MEDIAN}ms exceeds $2ms"
		EXIT_CODE=$(($EXIT_CODE+1))
	fi
	shift 2
done

exit $EXIT_CODE
