#!/bin/bash -x

# Updates probes execution duration and polling period in a test plan

usage()
{
  printf "Usage: %s <ctp_file> <test_duration_s> <probe_period_ms> \n" "`basename $0`"
}

if test "$1" = -h -o "$1" = --help
then
  usage "$0"
  exit 0
elif test $# -lt 2
then
  printf "Missing required arguments.\n" >&2
  usage "$0"
  exit -1
fi

# Set duration test for probes
sed -i "s/argument=[0-9]* [0-9]*\ */argument=$3 $2 /" $1
