#!/bin/bash -x

# Remotely starts CLIF servers necessary to deploy given test plans, using ssh
# connections. Existing CLIF servers with the same names on the same hosts
# are killed and overridden in the CLIF registry.
# Important:
# - SSH access must be granted without password (connection with ssh keys)
# - CLIF server names in the test plans must match IP addresses or
#   properly resolved hostnames.
# Prerequesite: the CLIF registry must be running prior to running this script.

REMOTE_USER=clif
SSH_OPTS="-q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

usage()
{
	printf "Usage: %s <registry address> <clif_testplan_file.ctp>...\n" "`basename $0`"
}

if test "$1" = -h -o "$1" = --help
then
	usage "$0"
	exit 0
elif test $# -lt 2
then
	printf "Missing at least 2 required arguments.\n" >&2
	usage "$0"
	exit -1
fi

REGISTRY_ADDR="$1"
shift
for s in $(clifcmd listservers $@ |grep -v "local host")
do
	ssh $SSH_OPTS "$REMOTE_USER@$s" pkill -f "ClifServerImpl\ $s"
	ssh $SSH_OPTS "$REMOTE_USER@$s" rm -f "$s.out" "$s.err"
	ssh $SSH_OPTS "$REMOTE_USER@$s" bin/clifcmd config "$REGISTRY_ADDR"
	while IFS=' ' read -ra ADDR; do
		for i in "${ADDR[@]}"; do
			var="${i%%=*}"
			ssh $SSH_OPTS "$REMOTE_USER@$s" "sed -i '/^$var=/d' clif.opts ; echo $i >> clif.opts"
		done
    done <<< "$JAVA_OPTS"
	ssh $SSH_OPTS "$REMOTE_USER@$s" nohup bin/clifcmd server "$s" >"$s.out" 2>"$s.err" &
done
clifcmd waitservers "$@"
