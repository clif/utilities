#!/bin/bash

# Locally starts CLIF servers necessary to deploy given test plans, overriding
# any pre-existing CLIF server with the same name.
# This script makes it easy to test the deployment of a distributed test plan
# over a single host.
# Prerequesite: the CLIF registry must be running prior to running this script.

usage()
{
    printf "Usage: %s <clif_testplan_file.ctp>...\n" "`basename $0`"
}

if test $# -eq 0
then
    printf "Missing required arguments: test plan file(s).\n" >&2
    usage "$0"
    exit -2
elif test "$1" = -h -o "$1" = --help
then
    usage "$0"
    exit 0
fi

clifcmd listservers "$@" | while read s
do
    if test "$s" != "local host"
    then
        pkill -f ClifServerImpl\ "$s"
        rm -f "$s.out" "$s.err"
        clifcmd server "$s" >"$s.out" 2>"$s.err" &
        while ! test -s "$s.out" -o -s "$s.err"
        do
            sleep 1
        done
        if ! grep "CLIF server $s is ready." "$s.out"
        then
            printf "Failed to create CLIF server %s:\n" "$s" >&2
            cat "$s.err" >&2
            exit -3
        fi
    fi
done
