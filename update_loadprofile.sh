#!/bin/bash -x

# Update the load profile of an ISAC scenario

usage()
{
  printf "Usage: %s <xis_filename> <behavior_name> <force_stop> <time_s_1> <nb_vUser_1> <time_s_2> <nb_vUser_2> ... \n" "`basename $0`"
}

if test "$1" = -h -o "$1" = --help
then
  usage "$0"
  exit 0
elif test $# -lt 7
then
  printf "Missing required arguments.\n" >&2
  usage "$0"
  exit -1
fi

NB_POINT=$(( (($# -3) /2) -1 ))
TEXT=""
echo $NB_POINT
for n in $(seq 1 $NB_POINT);
do
  i=$(( 2+ (2*$n) ))
  j=$(( 3+ (2*$n) ))
  k=$(( 4+ (2*$n) ))
  l=$(( 5+ (2*$n) ))
  TEXT="$TEXT  <ramp style=\"line\">\n\
              <points>\n\
                <point x=\"${!i}\" y=\"${!j}\"></point>\n\
                <point x=\"${!k}\" y=\"${!l}\"></point>\n\
             </points>\n\
            </ramp>"
done

echo $TEXT

# Update load loadprofile
sed -i "/<group behavior=\"$2\"/,/<\/group>/c\
\         <group behavior=\"$2\" forceStop=\"$3\">\n\
          $TEXT\n\
          </group>\
" $1
