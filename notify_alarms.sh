#!/bin/bash
#
# Checks for alarms occurrences from the lastest execution of a given test plan
# and sends an e-mail notification when alarms are found.
#
# arguments:
# - test plan path/name without .ctp extension
# - e-mail recipient(s) address(es)

TESTPLAN="$1"
shift
ALARMS=$(./check_alarms.sh "$TESTPLAN" 0)
EXITCODE=$?
SUBJECT="CLIF test plan ${TESTPLAN} raised ${ALARMS} alarms"

if test $EXITCODE -ne 0
then
    LATEST_ACTION_FILE=$(find report -name action | grep "${TESTPLAN}" | sort -r | head -n 1)
	printf "%s\n\n%s\n" "$SUBJECT" "$ALARMS" | mailx -s "$SUBJECT" --attach "$LATEST_ACTION_FILE" "$@"
fi
exit $EXITCODE
