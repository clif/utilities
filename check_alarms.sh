#!/bin/bash

# Looks for the latest CLIF test execution (in standard report directory)
# and returns (as process exit code) the total number of alarms produced by the
# given injectors/probes

usage()
{
    printf "Usage: %s test_name bladeId1 [bladeId2 ...]\n" $1
    printf "Checks for existence of alarm files in the latest test, for given blades of a given test.\n"
}

if test $# -lt 2
then
    usage $(basename "$0")
    exit -2
fi

cd $(find report -mindepth 1 -maxdepth 1 -type d -regextype sed -regex "^report/${1}_[2-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]h[0-9][0-9]m[0-9][0-9]" |sort|tail -1)
let alarms=0
shift
for bladeId in $@
do
    if test -f "$bladeId/alarm"
    then
        cat "$bladeId/alarm"
        let alarms+=$(cat "$bladeId/alarm" | grep -v '#' | wc -l)
    fi
done

if test $alarms -gt 0
then
    printf "Test failed with %s alarm(s).\n" $alarms
    exit -1
else
    printf "Test passed without alarms.\n"
    exit 0
fi
