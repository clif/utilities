Shell script utilities for CLIF
===============================
Miscellaneous Bash scripts...
  - start/stop of local or remote CLIF servers
  - results analysis and alerting
  - test plans and scenarios generation
