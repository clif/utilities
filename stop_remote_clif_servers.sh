#!/bin/bash

# Remotely kills CLIF servers used by given test plans, using ssh
# connections.
# Important:
# - SSH access must be granted without password (connection with ssh keys)
# - CLIF server names in the test plans must match IP addresses or
#   properly resolved hostnames.
# Prerequesite: the CLIF registry must be running prior to running this script.

REMOTE_USER=clif
SSH_OPTS="-q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

usage()
{
	printf "Usage: %s <clif_testplan_file.ctp>...\n" "`basename $0`"
}

if test "$1" = -h -o "$1" = --help
then
	usage "$0"
	exit 0
elif test $# -eq 0
then
	printf "Missing at least 1 required argument.\n" >&2
	usage "$0"
	exit -1
fi

for s in $(clifcmd listservers "$@")
do
	if test "$s" != "local host"
	then
		ssh $SSH_OPTS "$REMOTE_USER@$s" pkill -ef "ClifServerImpl\ $s"
	fi
done
